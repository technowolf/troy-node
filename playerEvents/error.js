/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

module.exports = (client, error, message, ...args) => {
    switch (error) {
    case 'NotPlaying':
        message.channel.send(
            `${client.emotes.error} - There is no music being played on this server !`)
        break
    case 'NotConnected':
        message.channel.send(
            `${client.emotes.error} - You are not connected in any voice channel !`)
        break
    case 'UnableToJoin':
        message.channel.send(
            `${client.emotes.error} - I am not able to join your voice channel, please check my permissions !`)
        break
    case 'VideoUnavailable':
        message.channel.send(
            `${client.emotes.error} - ${args[0].title} is not available in your country! Skipping...`)
        break
    case 'MusicStarting':
        message.channel.send('The music is starting... please wait and retry!')
        break
    default:
        message.channel.send(
            `${client.emotes.error} - Something went wrong ... Error : ${error}`)
    }

}
