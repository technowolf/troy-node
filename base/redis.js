/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Redis = require('ioredis')
const { redisHost, redisPass } = require('../config.json')
const redis = new Redis({
    port: 6379,
    host: redisHost,
    enableReadyCheck: true,
    password: redisPass,
    db: 0,
})

module.exports = class RedisClient {
    static get db() {
        return redis
    }

    static start() {
        redis.on('connect', () => console.info('[REDIS][CONNECT]: Connecting...'))
        redis.on('ready', () => console.info('[REDIS][READY]: Ready!'))
        redis.on('error', error => console.error(`[REDIS][ERROR]: Encountered error:\n${error}`))
        redis.on('reconnecting', () => console.warn('[REDIS][RECONNECT]: Reconnecting...'))
    }
}
