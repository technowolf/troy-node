### Version 1.3.4
> - Dependencies Update
>   - Discord Player Extractor
>   - discord.js opus
>   - cheerio
>   - discord player
>   - eslint
>   - ioredis
>   - ffmpeg-static
>   - prism-media
>   - eslint-plugin-json (dev dependencies)
> - New functionality
>   - Wish Jay shree ram to #chit-chats for sunrise and sunset time of ayodhya.
> - Fixes/Enhancements
>   - code cleanup and general housekeeping.

### Version 1.3.3
> - Dependencies Added/Update
>   - Discord Player Extractor
>   - cheerio
>   - discord player
>   - eslint
>   - ioredis
>   - opusscript
>   - eslint-plugin-json (dev dependencies)
> - New Commands
>   - stop: stops whatever is playing.
>   - lyrics: fetches the lyrics for the given search query.
> - Fixes/Enhancements
>   - fixed music player to download audio only from yt-dl
>   - fix yodish commands where sending capitalized string breaks API.
>   - add rich embed for play command.
>   - removed poll command since it is not needed anymore. 
>   - code cleanup and general housekeeping.

### Version 1.3.2
> - Dependencies Update
>   - discordjs/opus
>   - Chalk
>   - cheerio
>   - discord-player
>   - Discord.js
>   - eslint
>   - node-superfetch
>   - semver
>   - statcord
> - New commands
>   - sike: Say sike right now!
>   - bulseet: When someone says something shitty, Reply with it a bulseet.
>   - its-our: Remind everyone that on a soviet soil, Everything is ours.
>   - sarno: Summons a sarcastic catto to say no.
>   - sorry-didi: Apologizes to woke didis out there.
> - Miscellaneous Changes
>   - Update Readme to add few more clarifications setting up the bot for the first time
>   - Add new activity status for bot.

### Version 1.3.1
> - Dependencies Update
>   - ffmpeg-static
> New commands: 
>   - Volume: Lets user control volume for the playback when bot is playing music.

### Version 1.3.0
> - Add new Dependencies:
>   - discordjs/opus
>   - discord-player
>   - ffmpeg-static
> - New Commands: 
>   - play: Play your query from youtube. Spotify is not supported for now.
>   - pause: Pauses the music playback
>   - filter: Lets you apply filters to music playback.
>   - now-playing: Shows the now playing summary with few extra meta about music.
>   - queue: Shows the current queue of tracks.
>   - resume: Resumes the music playback
>   - search: Lets you search the music on Youtube. Then reply with the number from 
>     index to queue the song corresponding to the number.
>   - clear-queue: Clears current queue.
>   - loop: Sets the now playing track on repeat mode. If `queue` is supplied with 
>     the command e.g. `loop queue`, the current queue will be set on repeat mode.
>     Use loop again to disable it.
>   - skip: Skips the current track. Also, can be used as Next command.
> - General housekeeping and code cleanup.

### Version 1.2.3
> - Add eslint rule to throw error on semi-colons. (I come from heavy
>   kotlin and similar language that's why)
> - Update Readme with more clarity to set up and run Troy for your own
>   server.
> - New commands:
>   - know-your-meme: Queries knowyourmeme.com to search for meme
>   - npm: Searches for package info to NPM registry.
>   - stackoverflow: Search for your question on StackOverflow.com
>   - steam: Search for game info on Steam store.
>   - tick-tack-toe: Play tick-tac-toe game with others.
>   - dependency-update: Checks for any dependency update available. It
>     is an owner only command, so it won't work for other users.
>   - bunker: Fetches bunker unlock codes for COD: MW (Warzone) game.
> - New Dependency:
>   - semver: to aid dependency checking.
> - Bug fixes:
>   - Fixed bug in urban dictionary command when definition or example string 
>     size is more than 1024


### Version 1.2.2
> - Integrate StatCord for detailed metrics for bot.
> - Add logging events when message gets edited or deleted.
> - Bumped up the Node version requirement to migrate from LTS to Latest
>   Stable since some string related methods are not available in LTS
>   for now.
> - Usual housekeeping and code optimization.
> - New commands:
>   - Akinator: Troy is your new Akinator, Fully integrated with
>     akinator API.
>   - Gravatar: Get the Gravatar for any Email. Also filters **'R'**
>     rated images.
>   - MDN: Fetches doc/article from Mozilla Developer Network for given
>     query.
>   - Roulette: Allows user to play Roulette with Troy. Not russian
>     roulette though, Your old school casino roulette.
>     Russian Roulette: Allows 2 players play infamous Russian Roulette.
>     But worry not, In DM, Troy will play Russian Roulette with you.
> - Add new dependencies
>   - cheerio: Helps to get DOM elements with jQuery on node.
>   - discordjs/collection: Utility DS for discord.js
>   - aki-api: Akinator API wrapper for Node.

### Version 1.2.1
> - Add Structure for Bot client and Commands
> - Introduce Redis DB for storing things in database
> - Introduce 4 custom argument types for ease while development
> - Updated commands:
>   - mute: Timeout has been added. Default will be 1 hour.
> - New Commands:
>   - reming: Reminds the command author something after the defined time
>   - delete-timer: Deletes the timer set by the author.

check commits for detailed info on changes.

### Version 1.2.0
> - Update overhaul changes to bot architecture
> - Introduce discord.js-commando library to ease the development.
> - Organize commands in groups.
> - Introduce new commands
>   - noods
>   - yoda
> - Remove existing commands
>   - poll (Temporary removal, It will be added later)
>   - args-info
>   - whoami

### Version 1.1.0
> - Commands made around fun and moderation.

### Version 1.0
> - Initial Repo