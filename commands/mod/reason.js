/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { getAdministratorRole } = require('../../utils/getAdministratorRole')
const { getModerationChannel } = require('../../utils/getModerationChannel')

module.exports = class ReasonCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'reason',
            description: 'Updates reason for previously issued mod actions',
            group: 'mod',
            memberName: 'reason',
            examples: ['reason <case-no> <reason>'],
            args: [
                {
                    key: 'caseNumber',
                    prompt: 'Please specify case number.',
                    type: 'integer',
                }, {
                    key: 'reason',
                    prompt: 'Please state the reason',
                    type: 'string',
                },
            ],
        })

    }

    hasPermission(message) {
        const adminRole = getAdministratorRole(message)
        return message.member.roles.cache.has(adminRole.id) || this.client.isOwner(message.author)
    }

    async run(message, { caseNumber, reason }) {
        const logChannel = await getModerationChannel(message)
        if (!logChannel) return message.channel.send('Cant find moderation log channel on server!')

        await logChannel.messages.fetch({ limit: 100 }).then((messages) => {
            const caseLog = messages.filter(filterMessage => filterMessage.author.id === message.client.user.id &&
                filterMessage.embeds[0] &&
                filterMessage.embeds[0].type === 'rich' &&
                filterMessage.embeds[0].footer &&
                filterMessage.embeds[0].footer.text.startsWith('Case') &&
                filterMessage.embeds[0].footer.text === `Case ${caseNumber}`,
            ).first()
            logChannel.messages.fetch(caseLog.id).then(logMsg => {
                const embed = logMsg.embeds[0]
                updateEmbed(embed)
                const reasonField = embed.fields.find(field => field.name === 'Reason')
                reasonField.value = reason
                logMsg.edit({ embed }).then(() => {
                    message.channel.send(`Updated the reason for case number ${caseNumber}`)
                })
            })
        })
    }
}

async function updateEmbed(embed) {
    embed.message ? delete embed.message : null
    embed.footer ? delete embed.footer.embed : null
    embed.provider ? delete embed.provider.embed : null
    embed.thumbnail ? delete embed.thumbnail.embed : null
    embed.image ? delete embed.image.embed : null
    embed.author ? delete embed.author.embed : null
    embed.fields ? embed.fields.forEach(f => {
        delete f.embed
    }) : null
    return embed
}