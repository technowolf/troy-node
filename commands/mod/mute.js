/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const { getAdministratorRole } = require('../../utils/getAdministratorRole')
const { getModerationChannel } = require('../../utils/getModerationChannel')
const { caseNumber } = require('../../utils/caseNumber')
const { prefix } = require('../../config.json')
const { Constants } = require('discord.js')
const ms = require('ms')

module.exports = class MuteCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'mute',
            description: 'Mutes a user.',
            group: 'mod',
            memberName: 'mute',
            examples: ['mute <user> [time] [reason]'],
            aliases: ['softban', 'silence'],
            args: [
                {
                    key: 'user',
                    prompt: 'Whom do you want to mute?',
                    type: 'user',
                }, {
                    key: 'time',
                    prompt: 'How long do you want to mute?',
                    type: 'string',
                    default: '1h',
                }, {
                    key: 'reason',
                    prompt: 'Please state the reason',
                    type: 'string',
                    default: '',
                },
            ],
            clientPermissions: ['EMBED_LINKS'],
        })

    }

    hasPermission(message) {
        const adminRole = getAdministratorRole(message)
        return message.member.roles.cache.has(adminRole.id) || this.client.isOwner(message.author)
    }

    async run(message, { user, reason, time }) {

        if (this.client.isOwner(user)) return message.send('You can\'t hurt gods. Not here.')

        const logChannel = await getModerationChannel(message)
        if (!logChannel) return message.channel.send('Cant find moderation log channel on server!')

        const caseNo = await caseNumber(message.client, logChannel)
        const muteRole = message.guild.roles.cache.find(role => role.name === 'Muted')

        if (!reason) reason = `Awaiting moderator's input. Use ${prefix}reason ${caseNo} <reason>`

        const timeout = ms(time)

        // noinspection JSUnresolvedVariable,JSUnresolvedFunction
        const muteEmbed = new MessageEmbed()
            .setColor('RED')
            .setAuthor(`Case ${caseNo} | Mute | ${user.username}#${user.discriminator}`, user.avatarURL())
            .addFields(
                { name: 'User', value: `${user.username}#${user.discriminator}`, inline: true },
                { name: 'Moderator', value: `${message.author.toString()}`, inline: true },
                { name: 'Reason', value: reason, inline: true },
            )
            .setFooter(`Case ${caseNo}`)
            .setTimestamp()

        if (message.guild.member(user).roles.cache.has(muteRole.id)) {
            return message.channel.send('Dumbfuck, That user is already muted.')
        }

        await message.guild.member(user).roles.add(muteRole).then(member => {
            message.channel.send(`Successfully Muted ${member.user.toString()}.\nhttps://i.giphy.com/media/HN7yFdZ978jok/giphy.gif`)
            logChannel.send(muteEmbed)
        }).catch(error => {
            if (error.code === Constants.APIErrors.MISSING_PERMISSIONS) {
                return message.channel.send('I don\'t have permission to Mute mentioned user.')
            }
        })
        setTimeout(() => {
            message.guild.member(user).roles.remove(muteRole)
                .then(() => {
                    message.channel.send(`${message.author.toString()}, Bailed ${user.toString()} from the lockup.`)
                })
        }, timeout)
    }
}