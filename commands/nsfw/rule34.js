/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const fetch = require('node-fetch')

// noinspection SpellCheckingInspection
module.exports = class Rule34 extends Command {
    constructor(client) {
        super(client, {
            name: 'rule34',
            description: 'Provides Rule34 Content',
            group: 'nsfw',
            memberName: 'rule34',
            nsfw: true,
            examples: ['rule34 <query>'],
            aliases: ['rule34', 'rl34', '34'],
            throttling: { usages: 1, duration: 5 },
            args: [
                {
                    key: 'search',
                    prompt: 'What do you want to see?',
                    type: 'string',
                },
            ],
            credit: [{
                name: 'Mayank Malik',
                url: 'https://gitlab.com/mostwanted002',
                reason: 'Codebase',
            }],
            clientPermissions: ['EMBED_LINKS'],
        })

    }

    async run(message, { search }) {
        const nsfwRole = message.guild.roles.cache.find(role => role.name === 'Ashleel')
        if (!message.member.roles.cache.has(nsfwRole.id)) return message.channel.send('Shushh! you normie underage aggiN. NOT ALLOWED by the orders of Pitamaha! 😤')

        const rule34 = await fetch('https://rule34.xxx/index.php?page=dapi&s=post&q=index&limit=1000&json=1&tags=' + search)
            .then(response => response.json())
            .catch(console.error)
        if (rule34 === undefined) {
            message.channel.send('No Results found!').catch(console.error)
        }
        else {

            const lucky = Math.floor(Math.random() * Math.floor(rule34.length))
            const noodEmbed = new MessageEmbed()
                .setColor('RANDOM')
                .setTitle('Here\'s some fine piece of art as requested.')
                .setDescription('Stay horny 🥵')
                .setImage(rule34[lucky].file_url)
                .setFooter(`Powered by ${message.client.user.username}`, message.client.user.avatarURL())
                .setTimestamp()
            await message.react('😈').then(() => {
                message.channel.send(noodEmbed).catch(console.error)
            })
        }
    }
}