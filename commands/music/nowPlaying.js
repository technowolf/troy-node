/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')

module.exports = class NowPlayingCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'now-playing',
            aliases: ['np'],
            guildOnly: true,
            group: 'music',
            memberName: 'now-playing',
            description: 'Displays currently playing music.',
        })
    }

    async run(message) {
        if (!message.member.voice.channel) {
            return message.channel.send(`${message.client.emotes.error} - You're not in a voice channel !`)
        }

        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) {
            return message.channel.send(`${message.client.emotes.error} - You are not in the same voice channel !`)
        }

        if (!message.client.player.getQueue(message)) {
            return message.channel.send(`${message.client.emotes.error} - No music currently playing !`)
        }

        const track = message.client.player.nowPlaying(message)
        const filters = []

        Object.keys(message.client.player.getQueue(message).filters).forEach((filterName) => message.client.player.getQueue(message).filters[filterName]) ? filters.push(message.filterName) : false

        message.channel.send({
            embed: {
                color: 'GOLD',
                author: { name: track.title },
                footer: { text: `Powered by ${message.client.user.username}` },
                fields: [
                    { name: 'Channel', value: track.author, inline: true },
                    { name: 'Requested by', value: track.requestedBy.username, inline: true },
                    { name: 'From playlist', value: track.fromPlaylist ? 'Yes' : 'No', inline: true },

                    { name: 'Views', value: track.views, inline: true },
                    { name: 'Duration', value: track.duration, inline: true },
                    { name: 'Filters activated', value: filters.length + '/' + message.client.filters.length, inline: true },

                    { name: 'Volume', value: message.client.player.getQueue(message).volume, inline: true },
                    { name: 'Repeat mode', value: message.client.player.getQueue(message).repeatMode ? 'Yes' : 'No', inline: true },
                    { name: 'Currently paused', value: message.client.player.getQueue(message).paused ? 'Yes' : 'No', inline: true },

                    { name: 'Progress bar', value: message.client.player.createProgressBar(message, { timecodes: true }), inline: false },
                ],
                thumbnail: { url: track.thumbnail },
                timestamp: new Date(),
            },
        })
    }
}