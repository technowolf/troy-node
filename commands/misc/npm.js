/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const moment = require('moment')
const { MessageEmbed } = require('discord.js')
const request = require('node-superfetch')
const { trimArray } = require('../../utils/Helper')

module.exports = class NPMCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'npm',
            group: 'misc',
            memberName: 'npm',
            description: 'Fetches NPM package info.',
            clientPermissions: ['EMBED_LINKS'],
            credit: [
                {
                    name: 'npm',
                    url: 'https://www.npmjs.com/',
                    reason: 'API',
                },
            ],
            args: [
                {
                    key: 'pkg',
                    label: 'package',
                    prompt: 'Which package would you like search?',
                    type: 'string',
                    parse: pkg => encodeURIComponent(pkg.replaceAll(' ', '-')),
                },
            ],
        })
    }

    async run(message, { pkg }) {
        try {
            const { body } = await request.get(`https://registry.npmjs.com/${pkg}`)
            if (body.time.unpublished) return message.say('This package no longer exists.')
            const version = body.versions[body['dist-tags'].latest]
            const maintainers = trimArray(body.maintainers.map(user => user.name))
            const dependencies = version.dependencies ? trimArray(Object.keys(version.dependencies)) : null
            const embed = new MessageEmbed()
                .setColor('RANDOM')
                .setAuthor('NPM', 'https://i.imgur.com/ErKf5Y0.png', 'https://www.npmjs.com/')
                .setTitle(body.name)
                .setURL(`https://www.npmjs.com/package/${pkg}`)
                .setDescription(body.description || 'No description.')
                .addField('❯ Version', body['dist-tags'].latest, true)
                .addField('❯ License', body.license || 'None', true)
                .addField('❯ Author', body.author ? body.author.name : '???', true)
                .addField('❯ Creation Date', moment.utc(body.time.created).format('MM/DD/YYYY h:mm A'), true)
                .addField('❯ Modification Date', moment.utc(body.time.modified).format('MM/DD/YYYY h:mm A'), true)
                .addField('❯ Main File', version.main || 'index.js', true)
                .addField('❯ Dependencies', dependencies && dependencies.length ? dependencies.join(', ') : 'None')
                .addField('❯ Maintainers', maintainers.join(', '))
            return message.embed(embed)
        }
        catch (err) {
            if (err.status === 404) return message.say('Could not find any results.')
            return message.reply(`Oh no, an error occurred: \`${err.message}\`. Try again later!`)
        }
    }
}