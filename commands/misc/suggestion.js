/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')

module.exports = class SuggestionCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'suggestion',
            description: 'Sends suggestion about bot to developers.',
            group: 'misc',
            throttling: { usages: 1, duration: 3600 },
            memberName: 'suggestion',
            examples: ['bug <bug report>'],
            aliases: ['suggest', 'feedback', 'advice'],
            args: [{
                key: 'suggestion',
                prompt: 'Please define the suggestion\nExample: Can you add a command to fight with someone?',
                type: 'string',
            }],
            clientPermissions: ['EMBED_LINKS'],
        })
    }

    run(message, { suggestion }) {

        const suggestionChannel = message.guild.channels.cache.find(channel => channel.name === 'troy-suggestion')
        if (!suggestionChannel) return message.channel.send('Can not find the suggestion channel. Something is wrong.')

        const suggestionEmbed = new MessageEmbed()
            .setColor('BLUE')
            .setTitle('Suggestion for Bot')
            .setDescription(`Suggested by ${message.author.username}(#${message.author.discriminator})`)
            .addField('Description', suggestion, false)
            .addField('Server Name', message.guild.name, true)
            .addField('Server ID', message.guild.id, true)
            .setFooter(`Powered by ${message.client.user.username}`, message.client.user.avatarURL())
            .setTimestamp()

        return suggestionChannel.send(suggestionEmbed).then(() => {
            message.reply('Thanks for submitting the suggestion.').catch(console.error)
        })
    }

}