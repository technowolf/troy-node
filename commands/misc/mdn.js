/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const request = require('node-superfetch')

module.exports = class MDNCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'mdn',
            group: 'misc',
            memberName: 'mdn',
            description: 'Searches MDN for your query.',
            clientPermissions: ['EMBED_LINKS'],
            credit: [
                {
                    name: 'Mozilla Developer Network Docs',
                    url: 'https://developer.mozilla.org/en-US/',
                    reason: 'API',
                },
            ],
            args: [
                {
                    key: 'query',
                    prompt: 'What article would you like to search for?',
                    type: 'string',
                    parse: query => query.replaceAll('#', '.prototype.'),
                },
            ],
        })
    }

    async run(message, { query }) {
        try {
            const { body } = await request
                .get('https://developer.mozilla.org/en-US/search.json')
                .query({
                    q: query,
                    locale: 'en-US',
                    highlight: false,
                })
            if (!body.documents.length) return message.say('Could not find any results.')
            const data = body.documents[0]
            const embed = new MessageEmbed()
                .setColor(0x066FAD)
                .setAuthor('MDN', 'https://i.imgur.com/DFGXabG.png', 'https://developer.mozilla.org/')
                .setURL(data.url)
                .setTitle(data.title)
                .setDescription(data.excerpt)
            return message.embed(embed)
        }
        catch (err) {
            return message.reply(`Oh crap, an error occurred: \`${err.message}\`. Try again later!`)
        }
    }
}