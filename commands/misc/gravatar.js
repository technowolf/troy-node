/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const request = require('node-superfetch')
const { hash } = require('../../utils/Helper')

module.exports = class GravatarCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'gravatar',
            group: 'misc',
            memberName: 'gravatar',
            description: 'Responds with the Gravatar for an email.',
            clientPermissions: ['ATTACH_FILES'],
            credit: [
                {
                    name: 'Gravatar',
                    url: 'https://en.gravatar.com/',
                    reason: 'API',
                    reasonURL: 'https://en.gravatar.com/site/implement/',
                },
            ],
            args: [
                {
                    key: 'email',
                    prompt: 'What email do you want to get Gravatar for?',
                    type: 'string',
                    parse: email => email.toLowerCase(),
                },
            ],
        })
    }

    async run(message, { email }) {
        const emailHash = hash(email, 'md5')
        try {
            const { body } = await request
                .get(`https://www.gravatar.com/avatar/${emailHash}`)
                .query({
                    size: 500,
                    default: 404,
                    rating: message.channel.nsfw ? 'r' : 'pg',
                })
            return message.say({ files: [{ attachment: body, name: `${emailHash}.jpg` }] })
        }
        catch (err) {
            if (err.status === 404) return message.say('Could not find any results.')
            return message.reply(`Oh crap, an error occurred: \`${err.message}\`. Try again later!`)
        }
    }
}
