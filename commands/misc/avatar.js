/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const Discord = require('discord.js')

module.exports = class AvatarCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'avatar',
            description: 'Get the avatar URL of the tagged user(s), or your own avatar.',
            throttling: {
                usages: 2,
                duration: 10,
            },
            group: 'misc',
            memberName: 'avatar',
            examples: ['avatar [user(s)]'],
            aliases: ['icon', 'pfp', 'dp'],
            clientPermissions: ['EMBED_LINKS'],
        })
    }

    run(message) {
        const avatarEmbed = new Discord.MessageEmbed()
            .setTimestamp()
            .setFooter()
            .setFooter(`Powered by ${this.client.user.username}`, this.client.user.avatarURL())

        if (!message.mentions.users.size) {
            avatarEmbed.setTitle('Your avatar')
            avatarEmbed.setURL(message.author.avatarURL())
            avatarEmbed.setImage(message.author.avatarURL({ dynamic: true }))
            return message.channel.send(avatarEmbed)
        }

        message.mentions.users.map(user => {
            avatarEmbed.setTitle(`${user.username}'s Avatar`)
            avatarEmbed.setURL(message.author.avatarURL())
            avatarEmbed.setImage(user.avatarURL({ dynamic: true }))
            return message.channel.send(avatarEmbed)
        })
    }
}