/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const moment = require('moment')
const { MessageEmbed } = require('discord.js')
const request = require('node-superfetch')
const { formatNumber, embedURL } = require('../../utils/Helper')
const { stackoverflowApiKey } = require('../../config.json')

module.exports = class StackOverflowCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'stack-overflow',
            group: 'misc',
            memberName: 'stack-overflow',
            description: 'Searches StackOverflow for your question you have asked.',
            aliases:['stack', 'stackoverflow'],
            clientPermissions: ['EMBED_LINKS'],
            credit: [
                {
                    name: 'Stack Exchange',
                    url: 'https://stackexchange.com/',
                    reason: 'API',
                    reasonURL: 'https://api.stackexchange.com/docs',
                },
            ],
            args: [
                {
                    key: 'query',
                    prompt: 'What is your question?',
                    type: 'string',
                },
            ],
        })
    }

    async run(message, { query }) {
        try {
            const { body } = await request
                .get('http://api.stackexchange.com/2.2/search/advanced')
                .query({
                    page: 1,
                    pagesize: 1,
                    order: 'asc',
                    sort: 'relevance',
                    answers: 1,
                    q: query,
                    site: 'stackoverflow',
                    key: stackoverflowApiKey,
                })
            if (!body.items.length) return message.say('Could not find any results.')
            const data = body.items[0]
            const embed = new MessageEmbed()
                .setColor('RANDOM')
                .setAuthor('Stack Overflow', 'https://i.imgur.com/P2jAgE3.png', 'https://stackoverflow.com/')
                .setURL(data.link)
                .setTitle(data.title)
                .addField('❯ ID', data.question_id, true)
                .addField('❯ Asked by', embedURL(data.owner.display_name, data.owner.link), true)
                .addField('❯ Views', formatNumber(data.view_count), true)
                .addField('❯ Score', formatNumber(data.score), true)
                .addField('❯ Creation Date', moment.utc(data.creation_date * 1000).format('MM/DD/YYYY h:mm A'), true)
                .addField('❯ Last Activity',
                    moment.utc(data.last_activity_date * 1000).format('MM/DD/YYYY h:mm A'), true)
            return message.embed(embed)
        }
        catch (err) {
            return message.reply(`Oh crap, an error occurred: \`${err.message}\`. Try again later!`)
        }
    }
}
