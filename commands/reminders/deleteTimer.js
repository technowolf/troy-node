/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')

module.exports = class DeleteReminderCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'delete-reminder',
            aliases: ['delete-remind', 'delete-timer', 'del-reminder', 'del-remind', 'del-timer'],
            group: 'reminders',
            memberName: 'delete-reminder',
            description: 'Deletes your reminder.',
        })
    }

    async run(message) {
        const exists = await this.client.timers.exists(message.channel.id, message.author.id)
        if (!exists) return message.reply('🕰️ You do not have a timer set in this channel.')
        await this.client.timers.deleteTimer(message.channel.id, message.author.id)
        return message.say('🕰️ Your timer has been deleted.')
    }
}