/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const burns = require('../../data/burn.json')
const { getOwnerUser } = require('../../utils/getOwner')

module.exports = class BurnCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'burn',
            description: 'Burns mentioned user.',
            throttling: {
                usages: 2,
                duration: 10,
            },
            group: 'fun',
            memberName: 'burn',
            examples: ['burn <user>'],
            args: [
                {
                    key: 'user',
                    prompt: 'Which user do you want to light on fire?',
                    type: 'user',
                },
            ],
            credit: [{
                name: 'Abhay Malik',
                url: 'https://gitlab.com/Kingslayer47/',
                reason: 'Code and Idea',
            }],
        })
    }

    async run(message, { user }) {
        const owner = await getOwnerUser(message)
        const reply = burns[Math.floor(Math.random() * burns.length)]
        if (user === message.client.user) return message.reply(`Huh, Burn me? But, ${reply}`)
        if (user === owner && message.author !== owner) return message.channel.send('You can\'t hurt the god.')
        return message.channel.send(`${user.toString()}, ${reply}`)
    }
}