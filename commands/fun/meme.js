/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */


const Command = require('../../base/Command')
const { memes } = require('../../data/memes.json')
const { MessageEmbed } = require('discord.js')

module.exports = class MemeCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'meme',
            description: 'Returns meme if it exists in bot database',
            examples: ['meme <meme you want to see>'],
            aliases: ['maymay', 'mimi', 'memes', 'mamme'],
            throttling: {
                usages: 2,
                duration: 10,
            },
            group: 'fun',
            memberName: 'memes',
            args: [{
                key: 'search',
                prompt: 'Which meme you want to see?',
                type: 'string',
            }],
            clientPermissions: ['EMBED_LINKS'],
        })
    }

    run(message, { search }) {
        const requestedMeme = memes.find(function(element) {
            return element.name.includes(search)
        })
        if (!requestedMeme) {
            return message.channel.send('Sorry, That meme is not in database. ' +
                'You can use /suggest to request your meme template. Also you can give the link of the meme while suggesting if possible.')
        }

        const memeEmbed = new MessageEmbed()
            .setColor('RANDOM')
            .setTitle(requestedMeme.fullName)
            .setImage(requestedMeme.url)
            .setFooter(`Powered by ${message.client.user.username}`, message.client.user.avatarURL())
            .setTimestamp()

        return message.channel.send(memeEmbed).catch(console.error)
    }
}