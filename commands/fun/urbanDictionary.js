/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const querystring = require('querystring')
const fetch = require('node-fetch')

module.exports = class UrbanDictionaryCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'urban',
            description: 'Returns a definition from Urban Dictionary',
            examples: ['urban <query>'],
            aliases: ['ud'],
            throttling: {
                usages: 1,
                duration: 10,
            },
            group: 'fun',
            memberName: 'urban',
            args: [{
                key: 'search',
                prompt: 'What do you want to search at UrbanDictionary?',
                type: 'string',
            }],
            clientPermissions: ['EMBED_LINKS'],
        })
    }

    async run(message, { search }) {
        const query = querystring.stringify({ term: search })
        const { list } = await fetch(`http://api.urbandictionary.com/v0/define?${query}`)
            .then(response => response.json())
        if (!list.length) return message.channel.send(`Sorry, No results found for **${search}**.`)

        const definition = list[0].definition.substring(0, 1024)
        const example = list[0].example.substring(0, 1024)
        const urbanDictionaryEmbed = new MessageEmbed()
            .setColor('RANDOM')
            .setTitle('Urban Dictionary')
            .addFields(
                { name: search, value: definition },
                { name: 'Example:', value: example },
                { name: 'Added by', value: list[0].author, inline: true },
                { name: 'Written on', value: list[0].written_on, inline: true },
            )
            .setFooter(`Powered by ${message.client.user.username}`, message.client.user.avatarURL())
            .setTimestamp()

        return await message.channel.send(urbanDictionaryEmbed)
    }
}