/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')

module.exports = class PollCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'poll',
            description: 'Gives a poll for the options separated by "|".',
            throttling: {
                usages: 2,
                duration: 10,
            },
            group: 'fun',
            memberName: 'poll',
            examples: ['poll "Title for poll" this | that | this too | that too'],
            args: [
                {
                    key:'title',
                    prompt: 'Title for the poll',
                    type: 'string',
                },
                {
                    key: 'options',
                    prompt: 'Options for the poll separated by |',
                    type: 'string',
                }],
            clientPermissions: ['EMBED_LINKS'],
            credit: [{
                name: 'Abhay Malik',
                url: 'https://gitlab.com/Kingslayer47/',
                reason: 'Code and Idea',
            }],
        })
    }
    async run(message, { title, options }) {

        const reactions = ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣', '🔟']
        const pollOption = options.split('|')

        if (pollOption.length <= 1) return message.channel.send('Doofas, I need 2 commands to set up poll')
        if (pollOption.length > 10) return message.channel.send('Maximum 10 options are supported.')

        const pollEmbed = new MessageEmbed()
            .setColor('RANDOM')
            .setTitle(`Poll for ${title}`)
            .setFooter(`Poll by ${message.author.username}`, message.client.user.avatarURL())
        for (const option in pollOption) {
            if(pollOption[option] === '') return message.channel.send('You supplied blank value dumbfuck! Do not use separators unless you want to add options.')
            pollEmbed.addFields(
                { name: `Option ${parseInt(option) + 1}`, value: `${pollOption[option]}`, inline: true },
            )
        }
        message.channel.send(pollEmbed)
            .then(async sentEmbed => {
                for (const option in pollOption) {
                    await sentEmbed.react(reactions[parseInt(option)])
                }
            })
            .catch(()=>{
                return message.channel.send('One of the emoji got fucked up.\nReport bug with /bug command.')
            })

    }
}