/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')

module.exports = class SarNo extends Command {
    constructor(client) {
        super(client, {
            name: 'sarno',
            description: 'Summons a sarcastic catto to say no.',
            throttling: { usages: 2, duration: 10 },
            group: 'fun',
            memberName: 'sarno',
            examples: ['sarno'],
        })
    }

    run(message) {
        const imageUrl = 'https://cdn.discordapp.com/attachments/690818384321052714/829208012827787274/image0.png'
        const sarNo = new MessageEmbed()
            .setColor('RANDOM')
            .setImage(imageUrl)
            .setFooter('Powered by cute Catto')
            .setTimestamp()
        return message.channel.send(sarNo)
    }
}