/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const request = require('node-superfetch')
const cheerio = require('cheerio')
const { MessageEmbed } = require('discord.js')
const { shorten } = require('../../utils/Helper')

module.exports = class KnowYourMemeCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'know-your-meme',
            aliases: ['kym', 'meme-info', 'meme-search'],
            group: 'fun',
            memberName: 'know-your-meme',
            description: 'Searches KnowYourMeme.com for your query.',
            clientPermissions: ['EMBED_LINKS'],
            credit: [
                {
                    name: 'Know Your Meme',
                    url: 'https://knowyourmeme.com/',
                    reason: 'Meme Database',
                },
            ],
            args: [
                {
                    key: 'query',
                    prompt: 'Which meme would you like to search?',
                    type: 'string',
                },
            ],
        })
    }

    async run(message, { query }) {
        try {
            const location = await this.search(query)
            if (!location) return message.say('Can\'t find any results.')
            const data = await this.fetchMeme(location)
            const embed = new MessageEmbed()
                .setColor('DEFAULT')
                .setAuthor('Know Your Meme', 'https://i.imgur.com/WvcH4Z2.png', 'https://knowyourmeme.com/')
                .setTitle(data.name)
                .setDescription(shorten(data.description || 'No description found.'))
                .setURL(data.url)
                .setThumbnail(data.thumbnail)
            return message.embed(embed)
        }
        catch (err) {
            return message.reply(`Oh crap, an error occurred: \`${err.message}\`. Try again later!`)
        }
    }

    async search(query) {
        const { text } = await request
            .get('https://knowyourmeme.com/search')
            .query({ q: query })
        const $ = cheerio.load(text)
        const location = $('.entry-grid-body').find('tr td a').first().attr('href')
        if (!location) return null
        return location
    }

    async fetchMeme(location) {
        const { text } = await request.get(`https://knowyourmeme.com${location}`)
        const $ = cheerio.load(text)
        const thumbnail = $('a[class="photo left wide"]').first().attr('href')
            || $('a[class="photo left "]').first().attr('href')
            || null
        return {
            name: $('h1').first().text().trim(),
            url: `https://knowyourmeme.com${location}`,
            description: this.getMemeDescription($),
            thumbnail,
        }
    }

    getMemeDescription($) {
        const children = $('.bodycopy').first().children()
        let foundAbout = false
        for (let i = 0; i < children.length; i++) {
            const text = children.eq(i).text()
            if (foundAbout) {
                if (text) return text
            }
            else if (text === 'About') {
                foundAbout = true
            }
        }
        return null
    }
}