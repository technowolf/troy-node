/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const querystring = require('querystring')
const fetch = require('node-fetch')

module.exports = class YodishCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'yodish',
            description: 'Translate text to yodish.',
            examples: ['yodish <text>'],
            aliases: ['yd', 'yoda'],
            throttling: {
                usages: 1,
                duration: 10,
            },
            group: 'fun',
            memberName: 'yodish',
            args: [{
                key: 'search',
                prompt: 'What do you want to translate in to yodish?',
                type: 'string',
            }],
            credit: [{
                name: 'Abhay Malik',
                url: 'https://gitlab.com/Kingslayer47/',
                reason: 'Code and Idea',
            }],
        })
    }

    async run(message, { search }) {
        const query = querystring.stringify({ text: search.toLowerCase() })
        await fetch(`http://yoda-api.appspot.com/api/v1/yodish?${query}`, { method: 'GET' })
            .then(response => response.json())
            .then(json => {
                if (!json.yodish) return message.channel.send('Sorry, Yoda API seems to have some issues.')
                return message.channel.send(json.yodish)
            })

    }
}