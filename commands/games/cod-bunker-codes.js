/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const bunkerCodes = require('../../data/cod-bunker-codes.json')
const bunkers = ['prison', 'farmland', 'south-junkyard', 'north-junkyard', 'park', 'tv-station']

module.exports = class RouletteCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'bunker-code',
            group: 'games',
            memberName: 'bunker-code',
            description: 'Sends bunker code when requested.',
            args: [
                {
                    key: 'bunker',
                    prompt: 'Which bunker do you want to open?',
                    type: 'string',
                    oneOf: bunkers,
                },
            ],
        })
    }

    run(message, { bunker }) {
        switch (bunker) {
        case 'prison': {
            message.channel.send(bunkerCodes.prison)
            break
        }
        case 'farmland': {
            message.channel.send(bunkerCodes.farmland)
            break
        }
        case 'south-junkyard': {
            message.channel.send(bunkerCodes.southJunkyard)
            break
        }
        case 'north-junkyard': {
            message.channel.send(!bunkerCodes.northJunkyard)
            break
        }
        case 'park': {
            message.channel.send(bunkerCodes.park)
            break
        }
        case 'tv-station': {
            message.channel.send(bunkerCodes.tvStation)
            break
        }
        }
    }
}